#!/usr/bin/env python3

import serial

dat = []
for i in range(65536):
    dat.append(i % 256)
b_dat = bytes(dat)

ser = serial.Serial('/dev/ttyUSB0', 115200*8, timeout=None)
while True:
    ser.write(b_dat)
ser.close()

