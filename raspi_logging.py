#!/usr/bin/env python3

import serial

ser = serial.Serial('/dev/ttyAMA0', 8*115200, timeout=None)

with open('result.bin', "wb") as f:
    while True:
        line = ser.read()
        f.write(line)
    ser.close()

