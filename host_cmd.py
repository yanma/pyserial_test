#!/usr/bin/env python

import serial

ser = serial.Serial('/dev/ttyUSB1', 4800, timeout=None)

while True:
    line = ser.readline()
    print(line.decode(), end='')
ser.close()

